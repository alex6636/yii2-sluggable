<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   04.09.2017
 */

namespace alexs\yii2sluggable;
use yii\base\Behavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\Inflector;

class Sluggable extends Behavior
{
    public
        $attribute_from = 'title',
        $attribute_slug = 'slug',
        $immutable = false,
        $slug_suffix_separator = '-';

    public function events() {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE=>'beforeValidate',
        ];
    }

    public function beforeValidate() {
        /** @var ActiveRecord $Model */
        $Model = $this->owner;
        if ($this->immutable && $Model->{$this->attribute_slug}) {
            return;
        }
        $slug = $this->generateSlug($Model->{$this->attribute_from});
        $Model->{$this->attribute_slug} = $this->makeUniqueSlug($slug);
    }

    public function generateSlug($str) {
        return Inflector::slug($str);
    }
    
    public function makeUniqueSlug($slug, $suffix = '') {
        $i = 1;
        /** @var ActiveRecord $Model */
        $Model = $this->owner;
        do {
            /** @var ActiveQuery $Query */
            $Query = $Model::find()->where([$this->attribute_slug=>$slug . $suffix]);
            if (!$Model->isNewRecord) {
                $pk_column = $Model->tableSchema->primaryKey[0];
                $pk_value  = $Model->getPrimaryKey();
                $Query->andWhere($pk_column . '!=:pk_value', ['pk_value'=>$pk_value]);
            }
            if (!$Query->count()) {
                break;
            }
            $suffix = $this->slug_suffix_separator . (++ $i);
        } while (true);
        return $slug . $suffix;
    }
}

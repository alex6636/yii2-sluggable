<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   04.09.2017
 */

namespace alexs\yii2sluggable;
use yii\helpers\Inflector;

abstract class TransliterateSluggable extends Sluggable
{
    /** @return array */
    public abstract function getTransliterateTable();
    
    public function generateSlug($str) {
        return Inflector::slug(strtr($str, $this->getTransliterateTable()));
    }
}

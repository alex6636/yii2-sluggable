<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   04.09.2017
 */

namespace alexs\yii2sluggable;

class TransliterateRussianSluggable extends TransliterateSluggable
{
    /** @return array */
    public function getTransliterateTable() {
        return [
            'а'=>'a',  'А'=>'A',  'б'=>'b',   'Б'=>'B',   'в'=>'v',   'В'=>'V',
            'г'=>'g',  'Г'=>'G',  'д'=>'d',   'Д'=>'D',   'е'=>'e',   'Е'=>'E',
            'ё'=>'e',  'Ё'=>'E',  'ж'=>'zh',  'Ж'=>'ZH',  'з'=>'z',   'З'=>'Z',
            'и'=>'i',  'И'=>'I',  'й'=>'y',   'Й'=>'Y',   'к'=>'k',   'К'=>'K',
            'л'=>'l',  'Л'=>'L',  'м'=>'m',   'М'=>'M',   'н'=>'n',   'Н'=>'N',
            'о'=>'o',  'О'=>'O',  'п'=>'p',   'П'=>'P',   'р'=>'r',   'Р'=>'R',
            'с'=>'s',  'С'=>'S',  'т'=>'t',   'Т'=>'T',   'у'=>'u',   'У'=>'U',
            'ф'=>'f',  'Ф'=>'F',  'х'=>'h',   'Х'=>'H',   'ц'=>'ts',  'Ц'=>'TS',
            'ч'=>'ch', 'Ч'=>'CH', 'ш'=>'sh',  'Ш'=>'SH',  'щ'=>'sch', 'Щ'=>'SCH',
            'ъ'=>'y',  'Ъ'=>'Y',  'ы'=>'i',   'Ы'=>'I',   'ь'=>'',    'Ь'=>'',
            'э'=>'e',  'Э'=>'E',  'ю'=>'yu',  'Ю'=>'YU',  'я'=>'ya',  'Я'=>'YA',
        ];
    }
}

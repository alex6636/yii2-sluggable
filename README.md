# README #

Sluggabble extension for Yii2

```php
<?php
namespace app\models;
use alexs\yii2sluggable\Sluggable;
use yii\db\ActiveRecord;

class Article extends ActiveRecord
{
    // ...
    public function behaviors() {
        return [
            [
                'class'=>Sluggable::className(),
                'attribute_from'=>'title', // optional
                'attribute_slug'=>'slug',  // optional
            ],
            // ...
        ];
    }
```

```php
<?php
namespace app\models;
use alexs\yii2sluggable\TransliterateRussianSluggable;
use yii\db\ActiveRecord;

class Article extends ActiveRecord
{
    // ...
    public function behaviors() {
        return [
            [
                'class'=>TransliterateRussianSluggable::className(),
                'attribute_from'=>'title', // optional
                'attribute_slug'=>'slug',  // optional
            ],
            // ...
        ];
    }
```

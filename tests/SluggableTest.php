<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   11-Sep-17
 */

namespace alexs\yii2sluggable\tests;
use alexs\yii2phpunittestcase\DatabaseTableTestCase;
use alexs\yii2sluggable\Sluggable;

class SluggableTest extends DatabaseTableTestCase
{
    public function testSlug() {
        $Article = new Article;
        $Article->attachBehavior('Sluggable', [
            'class'=>Sluggable::className(),
        ]);
        $Article->setAttributes([
            'id'   =>1,
            'title'=>'First article',
            'text' =>'First article contents',
        ]);
        $Article->save();
        $this->assertEquals('first-article', $Article->slug);

        $Article->title = 'First article changed';
        $Article->save();
        $this->assertEquals('first-article-changed', $Article->slug);
    }

    public function testSlugImmutable() {
        $Article = new Article;
        $Article->attachBehavior('Sluggable', [
            'class'=>Sluggable::className(),
            'immutable'=>true,
        ]);
        $Article->setAttributes([
            'id'   =>1,
            'title'=>'First article',
            'text' =>'First article contents',
        ]);
        $Article->save();
        $this->assertEquals('first-article', $Article->slug);

        $Article->title = 'First article changed';
        $Article->save();
        $this->assertEquals('first-article', $Article->slug);
    }

    public function testSlugUnique() {
        $Article = new Article;
        $Article->attachBehavior('Sluggable', [
            'class'=>Sluggable::className(),
        ]);
        $Article->setAttributes([
            'id'   =>1,
            'title'=>'First article',
            'text' =>'First article contents',
        ]);
        $Article->save();

        $Article2 = new Article;
        $Article2->attachBehavior('Sluggable', [
            'class'=>Sluggable::className(),
        ]);
        $Article2->setAttributes([
            'id'   =>2,
            'title'=>'First article',
            'text' =>'Second article contents',
        ]);
        $Article2->save();
        $this->assertEquals('first-article-2', $Article2->slug);
    }

    protected function getTableName() {
        return 'article';
    }

    protected function getTableColumns() {
        return [
            'id'   =>'pk',
            'slug' =>'string NOT NULL',
            'title'=>'string NOT NULL',
            'text' =>'string NOT NULL',
        ];
    }
}

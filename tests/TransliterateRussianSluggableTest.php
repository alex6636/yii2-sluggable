<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   11-Sep-17
 */

namespace alexs\yii2sluggable\tests;
use alexs\yii2phpunittestcase\DatabaseTableTestCase;
use alexs\yii2sluggable\TransliterateRussianSluggable;

class TransliterateRussianSluggableTest extends DatabaseTableTestCase
{
    public function testSlug() {
        $Article = new Article;
        $Article->attachBehavior('Sluggable', [
            'class'=>TransliterateRussianSluggable::className(),
        ]);
        $Article->setAttributes([
            'id'   =>1,
            'title'=>'Первая статья',
            'text' =>'Содержимое первой статьи',
        ]);
        $Article->save();
        $this->assertEquals('pervaya-statya', $Article->slug);
    }

    protected function getTableName() {
        return 'article';
    }

    protected function getTableColumns() {
        return [
            'id'   =>'pk',
            'slug' =>'string NOT NULL',
            'title'=>'string NOT NULL',
            'text' =>'string NOT NULL',
        ];
    }
}
